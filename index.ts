import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineTitleComponent } from './src/timeline-title/timeline-title.component';
import { SlimScrollModule } from 'ng2-slimscroll';

export * from './src/timeline-title/timeline-title.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    TimelineTitleComponent,
   
  ],
  exports: [
    TimelineTitleComponent,
  
  ]
})
export class TimelineTitleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelineTitleModule,
      providers: []
    };
  }
}
