import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineTitleComponent } from './timeline-title.component';

describe('TimelineTitleComponent', () => {
  let component: TimelineTitleComponent;
  let fixture: ComponentFixture<TimelineTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
